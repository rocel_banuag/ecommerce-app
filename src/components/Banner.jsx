import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { sliderItems } from '../data/data';
import { mobile } from '../responsive';

const Container = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    position: relative; 
    overflow: hidden;
    ${mobile({ display: 'none'})} 
`;
const Wrapper = styled.div`
    margin-top: -10px;
    height: 100%;
    width: 100%;
    display: flex;
`
const Slide = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    background-color: #${props => props.bg}
`;
const ImgContainer = styled.div`
    height: 100%
    flex: 1;
`;
const Image = styled.img`
    height: 80%;
    margin-left: 10%;
`;
const InfoContainer = styled.div`
    flex: 1;
    padding: 215px;
`;

const Title = styled.h1`
    font-size: 70px;
    margin-right: 10%;
`
const Desc = styled.p`
    margin: 50px 0px;
    font-size: 20px;
    font-weight: 500;
    letter-spacing: 3px;
    margin-right: 10%;
`
const Button = styled.button`
    padding: 10px;
    font-size: 20px;
    background-color: transparent;
    cursor: pointer;
    margin-right: 10%;
    
`

export const Banner = () => {
  return (
    <Container>
      <Wrapper>
        {sliderItems.map((item) => (
            <Slide bg={item.bg} key={item.id}>
                <ImgContainer>
                    <Image src={item.img}/>
                </ImgContainer>
                <InfoContainer>
                    <Title>{item.title}</Title>
                    <Desc>{item.desc}</Desc>
                    <Button><Link to="/products" style={{textDecoration:"none", color:'black'}}>SHOP NOW</Link></Button>
                </InfoContainer>
            </Slide>
        ))}
      </Wrapper>
    </Container>
   ) ;
};
