import React from 'react';
import { useContext } from 'react';
import UserContext from '../UserContext';
import styled from 'styled-components';
// import { Badge } from '@material-ui/core';
// import { ShoppingCartOutlined } from '@material-ui/icons';
import { Link } from 'react-router-dom'
import { mobile } from '../responsive'

const Container = styled.div`
    height: 60px;
    ${mobile({ height: '50px'})}
`;
const Wrapper = styled.div`
    margin-top: 10px;
    padding: 10px 20px;
    display: flex;
    justify-content: space-between;
    ${mobile({ height: '10px 0px'})}
`;
const Left = styled.div`
    flex:1;
`;
const Logo = styled.h1`
    font-weight: bold;
    font-size: 20px;
    cursor: pointer; 
    ${mobile({ fontSize: '24px'})}   
`;

const Right = styled.div`
    flex:1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    ${mobile({ flex:2, justifyContent: 'center'})} 
`;

const MenuItem = styled.div`
    font-size: 14px;
    cursor: pointer;
    margin-left: 25px;
    ${mobile({ fontSize: '12px', marginLeft: '10px'})} 
` 

const Navbar = () => {
    const { user } = useContext(UserContext);
    return (
        <Container>
            <Wrapper>
                <Left>
                    <Logo><Link to='/'style={{ textDecoration: 'none', color: 'black'}}>Fashion Shop.</Link></Logo>
                </Left>
                <Right>
                    <MenuItem><Link to="/" style={{ textDecoration: 'none', color: 'black' }}>HOME</Link></MenuItem> 
                    <MenuItem><Link to="/products" style={{ textDecoration: 'none', color: 'black' }}>PRODUCT</Link></MenuItem>
                    
                    {(user.id !== null) ?                  
                    <MenuItem><Link to='/logout'style={{ textDecoration: 'none', color: 'black' }}>LOGOUT</Link></MenuItem>
                    :
                    <>
                    <MenuItem><Link to="/register" style={{ textDecoration: 'none', color: 'black' }}>REGISTER</Link></MenuItem>
                    <MenuItem><Link to="/login" style={{ textDecoration: 'none', color: 'black' }}>SIGN IN</Link></MenuItem>
                    </>
                    }
                    {/* <Badge className='ml-5' color="secondary" badgeContent={4}>
                        <ShoppingCartOutlined />
                    </Badge> */}
                </Right>
            </Wrapper>
        </Container>
    )
}

export default Navbar