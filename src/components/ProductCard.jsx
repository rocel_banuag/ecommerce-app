// import { Link } from 'react-router-dom';
// import React from 'react';
// import styled from 'styled-components';

// const Card = styled.div`
//     width: 25%;
//     margin-top: 2rem;
//     margin-left: 1rem;
//     display: block;
//     border: 1px solid blue;
// `
// const Title = styled.h1`
//     margin-left: 1rem;
// `
// const Subtitle = styled.p`
//     margin-left: 1rem;
// `
// const Text = styled.p`
//     margin-left: 1rem;
// `    

// export const ProductCard = ({productProp}) => {

//     const {_id, name, description, price} = productProp;

//     return (
//         <Card>
//             <Title>{name}</Title>
//             <Subtitle>Description:</Subtitle>
//             <Text>{description}</Text>
//             <Subtitle>Price:</Subtitle>
//             <Text>₱{price}</Text>
//             <Link className="btn btn-primary" to={`/products/${_id}`}>Add to Cart</Link>
//         </Card>
//     )
// }

import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import { useCart } from 'react-use-cart';
import React from 'react';

export const ProductCard = ({productProp}) => {
    // const {addItem } = useCart();
    // Deconstruct the course properties into their own variables
    const {_id, name, description, price} = productProp;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>₱{price}</Card.Text>
                <Link className="btn btn-primary" to={`/products/${_id}`}>Add to Cart</Link>
            </Card.Body>
        </Card>
    )
}


