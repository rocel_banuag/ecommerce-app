import React, { useContext, useState, useEffect } from 'react';
import UserView from '../components/UserView';
import { AdminView } from '../components/AdminView';
import UserContext from '../UserContext';

export const Products = () => {

  const { user } = useContext(UserContext);

  const [ products, setProducts ] = useState([]);

  const fetchData = () => {
    fetch(`https://afternoon-shelf-62598.herokuapp.com/api/products/all`)
    .then(res => res.json())
    .then(data => {
      setProducts(data);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {(user.isAdmin === true)
        ? <AdminView productsData={products} fetchData={fetchData}/>
        : <UserView productsData={products}/>
      }
    </>
  );
}
