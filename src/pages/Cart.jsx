import React from 'react';
import { useCart } from 'react-use-cart';

export const Cart = () => {
    const {setItemQuantity} = useCart();

    const handleSetItemQuantity = (e) =>{
        setItemQuantity(cartItem.sku, e.targert.value);
    }
    return(
        <input
            style={{width: 50}}
            className='border-solid border-2'
            type='number'
            value={cartItem.quantity}
            onChange={handleSetItemQuantity}
            min={0}
        />
    )
}