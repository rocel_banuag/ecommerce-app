import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import styled from 'styled-components';

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: url('https://images.pexels.com/photos/4173108/pexels-photo-4173108.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    padding: 20px;
    background-color: white;
    margin-right: 40%;
`

const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`

const Form = styled.form`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
`

const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 10px 0px 0px;
    padding: 10px;
`

const Button = styled.button`
    width: 30%;
    height: 45px;
    margin-top: 20px;
    border: none;
    padding: 15px 20px;
    background-color: #D3E3E2; 
    cursor: pointer;
`

export const Register = () => {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    const registerUser = (e) => {

        // Prevents page redirection via form submission
        e.preventDefault();

        setFirstName('');
		setLastName('');
        setEmail('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');

        fetch(`https://afternoon-shelf-62598.herokuapp.com/api/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch(`https://afternoon-shelf-62598.herokuapp.com/api/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    if(data === true){

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Beauty Care Shop!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'Something went wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);

    return (
        (user.id !== null) ?
            <Navigate to="/products" />
        :
            <Container>
                <Wrapper>
                <Title>CREATE AN ACCOUNT</Title>
                <Form onSubmit={(e) => registerUser(e)}>
                    <Input 
                        type="text" 
                        placeholder="Enter first name"
                        value={firstName} 
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />

                    <Input
                        type="text" 
                        placeholder="Enter last name"
                        value={lastName} 
                        onChange={e => setLastName(e.target.value)}
                        required
                    />

                    <Input 
                        type="email" 
                        placeholder="Enter email"
                        value={email} 
                        onChange={e => setEmail(e.target.value)}
                        required
                    />

                    <Input 
                        type="text" 
                        placeholder="Enter Mobile Number"
                        value={mobileNo} 
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                    
                    <Input 
                        type="password" 
                        placeholder="Password"
                        value={password1} 
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />

                    <Input 
                        type="password" 
                        placeholder="Verify Password"
                        value={password2} 
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />

                    { isActive ? 
                        <Button variant="primary" type="submit" id="submitBtn">
                            Submit
                        </Button>
                        : 
                        <Button variant="danger" id="submitBtn" disabled>
                            Submit
                        </Button>
                    }
                </Form>
                <Text>
                    Already have an account? <Link to="/register">Click Here</Link> to login.
                </Text>

                </Wrapper>
            </Container>
    )
    
}