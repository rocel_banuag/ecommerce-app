import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export const Logout = () => {
  const { unsetUser, setUser } = useContext(UserContext);

  //clears user info
  unsetUser();

  useEffect(() => {
    setUser({ id: null });
  });
  return <Navigate to="/login" />;
}
