import React, { useContext, useState, useEffect } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import styled from 'styled-components';

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: url('https://images.pexels.com/photos/4173108/pexels-photo-4173108.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');
    display: flex;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled.div`
    width: 25%;
    padding: 20px;
    background-color: white;
    margin-right: 40%; 
`

const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`

const Form = styled.form`
    display: flex;
    flex-direction: column;
`

const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 0px;
    padding: 10px;
`
const Button = styled.button`
    width: 20%;
    height: 40px;
    margin-top: 5px;
    border: none;
    padding: 15px 15px;
    background-color: #D3E3E2; 
    cursor: pointer;
    margin-bottom: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
`
const Text = styled.p`
    font-size: 15px;
`

export const Login = () => {

  const { user, setUser } = useContext(UserContext);
  const [ email, setEmail ] = useState('');
  const [ password, setPassword] = useState('');
  const navigate = useNavigate();
  const [ isActive, setIsActive ] = useState(true);

  function authenticate(e){
    e.preventDefault();

    fetch(`https://afternoon-shelf-62598.herokuapp.com/api/users/login`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data.accessToken !== 'undefined') {
        localStorage.setItem('token', data.accessToken);
        retrieveUserDetails(data.accessToken);

        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: 'Welcome to Beauty Care Shop'
        });
        if(user.isAdmin){
          navigate.push('/admin');
        }else{
          navigate.push('/product');
        }
      } else{
        Swal.fire({
          title: 'Wrong Email or Password',
          icon: 'error',
          text: 'Check your login details and try again.'
        });
      };
    })
      setEmail('');
      setPassword('');
  }

  const retrieveUserDetails = (token) => {
    fetch(`https://afternoon-shelf-62598.herokuapp.com/api/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin,
        email: email
      });
    });
  };

  useEffect(() => {
    if((email !== '' && password !== '')){
      setIsActive(true);
    } else{
      setIsActive(false);
    }
  }, [email, password])
  return (
    (user.id !== null) ?
      (user.isAdmin)
      ?
      <Navigate to='/admin'/>
      :
      <Navigate to='products'/>
      :
    <Container>
      <Wrapper>
         <Title>LOGIN YOUR ACCOUNT</Title>
      <Form onSubmit={(e) => authenticate(e)}>
          <Input type="email" 
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required />
  
          <Input type="password" 
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required />

        { isActive ? 
          <Button className='' variant="primary" type="submit" id="submitBtn">
            LOGIN
          </Button>
          : 
          <Button variant="danger" type="submit" id="submitBtn" disabled>
            LOGIN
          </Button>
          }
      </Form>
      <Text>
        Don't have an account yet? <Link to="/register">Click Here</Link> to register.
      </Text>

      </Wrapper>
    </Container>
    
  );
}
