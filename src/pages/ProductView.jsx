import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve any parameter or the courseId passed via the URL
	const { productId } = useParams();
	const { user } = useContext(UserContext);
	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const cart = (productId) => {

		fetch(`https://afternoon-shelf-62598.herokuapp.com/api/users/cart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				Swal.fire({
					title: "Successfully Added",
					icon: 'success',
				});

				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component
				navigate.push("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
				});

			}

		});

	};

	useEffect(()=> {

		fetch(`https://afternoon-shelf-62598.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);

	

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>₱{price}</Card.Text>
							<div className='quantity'>
								<button className='btn minus-btn disabled' type='button'>-</button>
								<input type="text" id='quantity' value='1'/>
								<button className='btn minus-btn' type='button'>+</button>
							</div>
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => cart(productId)}>Buy Now</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/products"></Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}