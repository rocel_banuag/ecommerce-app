import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import {Products} from './pages/Products';
import ProductView from './pages/ProductView';
import Home from './pages/Home';
import {Login} from './pages/Login';
import {Logout} from './pages/Logout';
import {Register} from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function App() {

    const [user, setUser] = useState({
		id: null,
		isAdmin: null
		//email: null
	});

    const unsetUser = () => {
		localStorage.clear();

		setUser({
			id: null,
			isAdmin: null
		});

    };

    useEffect(() => {

		fetch('https://afternoon-shelf-62598.herokuapp.com/api/users/details', {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			// Set the user states values with the user details upon successful login.
			if (typeof data._id !== "undefined") {

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});

			// Else set the user states to the initial values
			} else {

				setUser({
					id: null,
					isAdmin: null
				});

			}

		})

    }, []);

	return (
		<UserProvider value={{user, setUser, unsetUser}}>
			<Router>
				<Navbar />
					<Routes>
						<Route exact path="/" element={<Home/>}/>
						<Route exact path="/products" element={<Products/>}/>
						<Route exact path="/products/:productId" element={<ProductView/>}/>
						<Route exact path="/login" element={<Login/>}/>
						<Route exact path="/logout" element={<Logout/>}/>
						<Route exact path="/register" element={<Register/>}/>
					</Routes>
			</Router>
		</UserProvider>
	);
}